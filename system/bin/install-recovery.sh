#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery:4673536:24417745d4aa48c803e034fe99d27f49409e17e9; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/mtk-msdc.0/by-name/boot:4046848:a9d1323e26a889a23e39c3b2f1e8b23911388845 EMMC:/dev/block/platform/mtk-msdc.0/by-name/recovery 24417745d4aa48c803e034fe99d27f49409e17e9 4673536 a9d1323e26a889a23e39c3b2f1e8b23911388845:/system/recovery-from-boot.p && echo "
Installing new recovery image: succeeded
" >> /cache/recovery/log || echo "
Installing new recovery image: failed
" >> /cache/recovery/log
else
  log -t recovery "Recovery image already installed"
fi
